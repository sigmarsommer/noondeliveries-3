<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<?php
if(is_page('Restaurant'))
{
?>
<div id="top"></div>
<div class="breadcrumb top_space_padding">

	<div class="container">

		<div class="sixteen columns">

	<ul>
		<li class="back-home donepage"><a href="">Home</a></li>
		<li class="onpage donepage"><a href="">Food Deliveries Punda</a></li>
	</ul>
</div>

</div>

</div>
<div class="location-header">

	<div class="container">

		<div class="sixteen columns location-head"><h4>Punda <span>food deliveries and takeaway</span></h4></div>

		<div class="sixteen columns location-sub-info"><span>36 restaurants</span> deliver in <span>Punda</span> <a href="">Change your location</a></div>

	</div>

</div>
<div class="venues">


	<div class="container">

		<div class="four columns">
		<ul class="venue-sections-list">

			<li class="selected"><a href="">All<span>36</span></a></li></ul>
				<?php wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_class' => 'venue-sections-list' ) ); ?>
				


			

		</div>




		<div class="twelve columns">

							<div class="sponsored">
							<?php
							$args = array( 'post_type' => 'restaurant', 'posts_per_page' => 10, 'texonomy'=>'cusion' );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();
    
?>


					<div class="v-data">



						<div class="tag-sponsored">sponsored</div>
						<!-- venue thumb --><div class="two columns thumb alpha"><?php set_post_thumbnail_size( 90, 86, true );
						the_post_thumbnail();
						 ?></div>

						<div class="seven columns venue-info">
							<h4><?php the_title(); ?></h4>
							<?php the_content(); ?>

							<div class="v-data-info1"><!-- venue delivery info -->
								<table>
									<tr class="title_head">
									  <th>Cuisines</th>
									  <th>Estimated Time</th>
									  <th>Delivery Costs</th>
									</tr>
									<tr>
									  <td>Drinks</td>
									  <td><?php echo get_post_meta( get_the_ID(), 'Estimated Time', true ); ?></td>
									  <td><?php echo get_post_meta( get_the_ID(), 'Delivery Costs', true ); ?></td>
									</tr>
								</table>
							</div>

						</div>


						<!-- button to menu page --><div class="three columns order-btn omega">

							<div class=" proceed button">order now<a href=""></a></div>

							<p class="reviews-box">Quality <span>( <a href="">10 reviews</a> )</span></p>

							<div class="rating"></div>

						</div>


					</div><!-- ======= Venue Data ======= -->
<?php endwhile;
							?>

					




</div><!-- not-sponsored -->







<!-- ========= Rest outside district ======== -->

<div class='item out_district '>Restaurants outside<span>district name</span>

	<div class="bouncedit">  <img class="hoverImages" src="<?php echo get_template_directory_uri(); ?>/images/layout-img/arrw.png" /></div>

</div>
<div class='item-data'><div>
<div class="v-data btm">



						
						<!-- venue thumb --><div class="two columns thumb alpha"><img src="<?php echo get_template_directory_uri(); ?>/images/layout-img/menu-icons/thumb.png"></div>

						<div class="seven columns venue-info">
							<h4>Liquor store @ Night</h4>
							<p>Pundaweg 12 (<span>Delivery Hours: 10:00 - 17:00</span>)</p>

							<div class="v-data-info"><!-- venue delivery info -->
								<table>
									<tr class="title_head">
									  <th>Cuisines</th>
									  <th>Estimated Time</th>
									  <th>Delivery Costs</th>
									</tr>
									<tr>
									  <td>Drinks</td>
									  <td>45 minutes</td>
									  <td>Free</td>
									</tr>
								</table>
							</div>

						</div>


						<!-- button to menu page --><div class="three columns order-btn omega">

							<div class=" proceed button">order now<a href=""></a></div>

							<p class="reviews-box">Quality <span>( <a href="">10 reviews</a> )</span></p>

							<div class="rating"><img src="<?php echo get_template_directory_uri(); ?>/images/layout-img/menu-icons/rating.png" alt="" ></div>

						</div>


</div><!-- ======= Venue Data ======= -->


<div class="v-data btm">



						
						<!-- venue thumb --><div class="two columns thumb alpha"><img src="<?php echo get_template_directory_uri(); ?>/images/layout-img/menu-icons/thumb.png"></div>

						<div class="seven columns venue-info">
							<h4>Liquor store @ Night</h4>
							<p>Pundaweg 12 (<span>Delivery Hours: 10:00 - 17:00</span>)</p>

							<div class="v-data-info"><!-- venue delivery info -->
								<table>
									<tr class="title_head">
									  <th>Cuisines</th>
									  <th>Estimated Time</th>
									  <th>Delivery Costs</th>
									</tr>
									<tr>
									  <td>Drinks</td>
									  <td>45 minutes</td>
									  <td>Free</td>
									</tr>
								</table>
							</div>

						</div>


						<!-- button to menu page --><div class="three columns order-btn omega">

							<div class=" proceed button">order now<a href=""></a></div>

							<p class="reviews-box">Quality <span>( <a href="">10 reviews</a> )</span></p>

							<div class="rating"><img src="<?php echo get_template_directory_uri(); ?>/images/layout-img/menu-icons/rating.png" alt="" ></div>

						</div>


</div><!-- ======= Venue Data ======= -->








</div></div>

<!-- ========= Rest outside discrict ======== -->









<!-- ========= Rest Close ======== -->

<div class='item'>Preorder now at closed restaurants
	<div class="bouncedit">  <img src="<?php echo get_template_directory_uri(); ?>/images/layout-img/arrw.png" /></div>
</div>
<div class='item-data'><div>
<div class="v-data btm">



						
						<!-- venue thumb --><div class="two columns thumb alpha"><img src="<?php echo get_template_directory_uri(); ?>/images/layout-img/menu-icons/thumb.png"></div>

						<div class="seven columns venue-info">
							<h4>Liquor store @ Night</h4>
							<p>Pundaweg 12 (<span>Delivery Hours: 10:00 - 17:00</span>)</p>

							<div class="v-data-info"><!-- venue delivery info -->
								<table>
									<tr class="title_head">
									  <th>Cuisines</th>
									  <th>Estimated Time</th>
									  <th>Delivery Costs</th>
									</tr>
									<tr>
									  <td>Drinks</td>
									  <td>45 minutes</td>
									  <td>Free</td>
									</tr>
								</table>
							</div>

						</div>


						<!-- button to menu page --><div class="three columns order-btn omega">

							<div class=" closed button">preorder<a href=""></a></div>

							<p class="reviews-box">Quality <span>( <a href="">10 reviews</a> )</span></p>

							<div class="rating"><img src="<?php echo get_template_directory_uri(); ?>/images/layout-img/menu-icons/rating.png" alt="" ></div>

						</div>


</div><!-- ======= Venue Data ======= -->


<div class="v-data btm">



						
						<!-- venue thumb --><div class="two columns thumb alpha"><img src="<?php echo get_template_directory_uri(); ?>/images/layout-img/menu-icons/thumb.png"></div>

						<div class="seven columns venue-info">
							<h4>Liquor store @ Night</h4>
							<p>Pundaweg 12 (<span>Delivery Hours: 10:00 - 17:00</span>)</p>

							<div class="v-data-info"><!-- venue delivery info -->
								<table>
									<tr class="title_head">
									  <th>Cuisines</th>
									  <th>Estimated Time</th>
									  <th>Delivery Costs</th>
									</tr>
									<tr>
									  <td>Drinks</td>
									  <td>45 minutes</td>
									  <td>Free</td>
									</tr>
								</table>
							</div>

						</div>


						<!-- button to menu page --><div class="three columns order-btn omega">

							<div class=" closed button">preorder<a href=""></a></div>


							<p class="reviews-box">Quality <span>( <a href="">10 reviews</a> )</span></p>

							<div class="rating"><img src="<?php echo get_template_directory_uri(); ?>/images/layout-img/menu-icons/rating.png" alt="" ></div>

						</div>


</div><!-- ======= Venue Data ======= -->

</div></div>

<!-- ========= Rest Close ======== -->







<div class="clear"></div>


<div class="suggest-btn">

	<h4>Suggest a Restaurant</h4>

<div class="green button">click here</div>
</div>


		</div><!-- end twelve colom -->

	
		</div><!-- list venues -->

	</div>
<?php
}
else
{
?>
<div id="main-content" class="main-content">
<?php
	if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>

<div id="primary" class="content-area">
   

    <?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
					//get_template_part( 'content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}
				endwhile;
				
			?>
  </div>
  
      <div class="suggest-btn">
    
        <h4>Suggest a Restaurant</h4>
    
        <div class="green button">click here</div>
    </div>
  
  

<?php

}
get_footer();
?>